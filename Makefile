# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
##   Updated: 2015/03/18 22:03:41 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

CC = g++

NAME = avm
LIBS = -lptools
LIBS_DIR = ptools

SOURCES_DIR = srcs
INCLUDES_DIR = includes

ifeq ($(DEBUG), 1)
	FLAGS = -g3 -Wall -Wextra
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra
endif

CFLAGS = $(foreach DIR, $(LIBS_DIR), -I ./$(DIR)/$(INCLUDES_DIR) )
CFLAGS += $(FLAGS) -I $(INCLUDES_DIR)

LDFLAGS = $(foreach DIR, $(LIBS_DIR), -L ./$(DIR) )
LDFLAGS += $(LIBS)

DEPENDENCIES = \
				$(INCLUDES_DIR)/Exception.hpp \
				$(INCLUDES_DIR)/RunTimeException.hpp \
				$(INCLUDES_DIR)/TOperations.hpp \
				$(INCLUDES_DIR)/TComparisons.hpp \
				$(INCLUDES_DIR)/OperandType.hpp \
				$(INCLUDES_DIR)/IOperand.hpp \
				$(INCLUDES_DIR)/TOperand.hpp \
				$(INCLUDES_DIR)/OperandFactory.hpp \
				$(INCLUDES_DIR)/Instruction.hpp \
				$(INCLUDES_DIR)/Lexer.hpp \
				$(INCLUDES_DIR)/Parser.hpp \
				$(INCLUDES_DIR)/Engine.hpp \
				$(INCLUDES_DIR)/Loader.hpp \
				$(INCLUDES_DIR)/Injector.hpp \

SOURCES = \
		  $(SOURCES_DIR)/main.cpp \
		  $(SOURCES_DIR)/Exception.cpp \
		  $(SOURCES_DIR)/RunTimeException.cpp \
		  $(SOURCES_DIR)/OperandFactory.cpp \
		  $(SOURCES_DIR)/Instruction.cpp \
		  $(SOURCES_DIR)/Lexer.cpp \
		  $(SOURCES_DIR)/Parser.cpp \
		  $(SOURCES_DIR)/Engine.cpp \
		  $(SOURCES_DIR)/Loader.cpp \
		  $(SOURCES_DIR)/Injector.cpp \

OBJS = $(SOURCES:.cpp=.o)

all: $(NAME)

%.o: %.cpp $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS) lib
	@echo Creating $(NAME)...
	@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

lib:
	@$(foreach dir, $(LIBS_DIR), make -C $(dir);)

test:
	@$(foreach DIR, $(LIBS_DIR), echo $(DIR);)

clean:
	@$(foreach dir, $(LIBS_DIR), make clean -C $(dir);)
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

fclean: clean
	@$(foreach dir, $(LIBS_DIR), make cleanbin -C $(dir);)
	@rm -f $(NAME)
	@rm -rf $(NAME).dSYM
	@echo Deleting $(NAME)...

re: fclean all

.PHONY: clean fclean re all test

