#ifndef INJECTOR_HPP
# define INJECTOR_HPP

# include <unistd.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <iostream>

# include "Exception.hpp"

namespace AVM
{
	namespace Injector
	{
		void				prepare(char const * fileName);
		extern	uint64_t	offset;
	}
}

#endif
