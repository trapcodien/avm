#ifndef PARSER_HPP
# define PARSER_HPP

# include <cctype>
# include <limits>
# include <map>
# include <algorithm>

# include "Instruction.hpp"
# include "RunTimeException.hpp"
# include "Lexer.hpp"

namespace AVM
{
	class Parser
	{
	private :
/******************************************************************************/
// Utils
/******************************************************************************/
		static std::string					trim(std::string const & str, char c);
		static std::string					extractRawValue(std::string const & value);
		static void							cleanTabulation(std::string & line);

/******************************************************************************/
// Checks
/******************************************************************************/
		static void							checkInt8(std::string const & raw);
		static void							checkInt16(std::string const & raw);
		static void							checkInt32(std::string const & raw);
		static void							checkFloat(std::string const & raw);
		static void							checkDouble(std::string const & raw);
		
/******************************************************************************/
// Main parsing functions
/******************************************************************************/
		static Instruction::type			getInstructionType(std::string const & cmd);
		static eOperandType					getOperandType(std::string const & value);
		static std::string					getInteger(std::string const & value);
		static std::string					getFloat(std::string const & value);
		
		static Instruction const *			createLabel(long numLine, std::string const & line);

/******************************************************************************/
// Public parsing functions
/******************************************************************************/
	public :
		static bool							isBlank(std::string const & line);
		static Instruction const *			createInstruction(long numLine, std::string & line);
	};
}

#endif /* !PARSER_HPP */
