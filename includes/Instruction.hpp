#ifndef INSTRUCTION_HPP
# define INSTRUCTION_HPP

# include <stdint.h>
# include <iostream>
# include <sstream>
# include "OperandType.hpp"

namespace AVM
{
	namespace Operations
	{
		template<typename T> T				strToType(std::string const & str);
		template<typename T> std::string	toStr(T value);
	}

	class Instruction
	{
	public :
		enum type { Push = 0, Assert, Pop, Dump, Add, Sub, Mul, Div, Mod, 
					Print, Exit, Putn, Put, Swap, Dup, Read, Cmp, Label,
					Jmp, Call, Je, Jne, Jlt, Jgt, Jle, Jge,
					Jc, Jnc, Jse, Jso, Js, Jeof, nbType };

	private :
		type			_type;
		std::string		_operandValue;
		eOperandType	_operandType;
		long			_line;
		std::string		_fileName;

/******************************************************************************/
// Private Coplien
/******************************************************************************/
		Instruction();

	public :
/******************************************************************************/
// Coplien
/******************************************************************************/
		~Instruction();
		Instruction(Instruction const & ref);
		Instruction &		operator=(Instruction const & rhs);

/******************************************************************************/
// Constructors
/******************************************************************************/
		Instruction(long line, type type);
		Instruction(long line, type type, std::string const & value, eOperandType valueType);
		Instruction(long line, type type, std::string const & label);

/******************************************************************************/
// Getters
/******************************************************************************/
		type				getType(void) const;
		std::string const &	getOperandValue(void) const;
		eOperandType		getOperandType(void) const;
		long				getLine(void) const;
		std::string const &	getFileName(void) const;

/******************************************************************************/
// Setter
/******************************************************************************/
		void				setFileName(std::string const & fileName);
		void				setOperandValue(unsigned int operandValue);
		
/******************************************************************************/
// Serialization / Unserialization
/******************************************************************************/
		void						operator>>(std::basic_ostream<char> & ofs) const;
		static Instruction const *	interpretBytecode(std::basic_istream<char> & ifs);
	};
}

#endif /* !INSTRUCTION_HPP */

