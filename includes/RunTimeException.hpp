#ifndef RUNTIME_EXCEPTION
# define RUNTIME_EXCEPTION

# include <iostream>
# include "Exception.hpp"

namespace AVM
{
	class RunTimeException : public Exception
	{
	public :
		enum type { 	BadValue = 6, Overflow, Underflow,
						EmptyStack, FloatingPoint, NoExit,
						Assert, InsufficientOperands };

		RunTimeException();
		~RunTimeException() throw();
		RunTimeException(RunTimeException const & ref);
		RunTimeException &				operator=(RunTimeException const & rhs);

		RunTimeException(RunTimeException::type error, long line = 0, std::string const & msgDetails = "");
	};
}

#endif /* !RUNTIME_EXCEPTION */
