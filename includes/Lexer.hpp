#ifndef LEXER_HPP
# define LEXER_HPP

# include <iostream>
# include <sstream>
# include <vector>

namespace AVM
{
	class Lexer
	{
	public :
		static std::vector<std::string>		split(std::string const & str, char delimiter);
	};
}

#endif /* !LEXER_HPP */
