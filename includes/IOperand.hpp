#ifndef I_OPERAND_HPP
# define I_OPERAND_HPP

# include <iostream>
# include "OperandType.hpp"

namespace AVM
{
	class IOperand {
	public:
		virtual int 				getPrecision( void ) const = 0;
		virtual eOperandType 		getType( void ) const = 0;

		virtual IOperand const *	operator+( IOperand const & rhs ) const = 0;
		virtual IOperand const *	operator-( IOperand const & rhs ) const = 0;
		virtual IOperand const *	operator*( IOperand const & rhs ) const = 0;
		virtual IOperand const *	operator/( IOperand const & rhs ) const = 0;
		virtual IOperand const *	operator%( IOperand const & rhs ) const = 0;

		virtual std::string const &	toString( void ) const = 0;

		virtual ~IOperand( void ) {}
	};
}

#endif /* !I_OPERAND_HPP */
