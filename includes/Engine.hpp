#ifndef ENGINE_HPP
# define ENGINE_HPP

# include <fstream>
# include <iostream>
# include <map>
# include <list>
# include <queue>
# include <stdint.h>
# include <sys/types.h>
# include <sys/stat.h>

# include "Parser.hpp"
# include "OperandFactory.hpp"

# define COLOR_RESET		"\033[0m"
# define COLOR_RED			"\033[31m"
# define COLOR_GREEN		"\033[32m"
# define COLOR_YELLOW		"\033[33m"

# define INJECTION_SIZE 	1048576

namespace AVM
{
	class Engine
	{
	public :
		static bool									verbose;
		static std::string							output;
		static long									wMagicBytecode;
		static long									rMagicBytecode;

		typedef std::list<Instruction const *>						InstructionQueue;
		typedef std::vector<IOperand const *>						OperandStack;
		typedef std::map<std::string, InstructionQueue::iterator>	LabelMap;
		typedef std::vector<InstructionQueue::iterator>				CallStack;

	private :
		enum CarryFlag
		{
			CF_NONE = 0x00,
			CF_LT = 0x01,
			CF_GT = 0x02,
			CF_EQ = 0x03
		};

		OperandFactory								_factory;
		OperandStack								_stack;
		InstructionQueue							_instructions;
		InstructionQueue::iterator					_cursor;
		LabelMap									_labels;
		CarryFlag									_carry;
		std::string									_readBuffer;
		CallStack									_callStack;

	public :
/******************************************************************************/
// Coplien
/******************************************************************************/
		Engine();
		~Engine();
		Engine(Engine const & ref);
		Engine &		operator=(Engine const & rhs);

/******************************************************************************/
// Main public functions
/******************************************************************************/
		static void	printVerbose(char const * msg);
		void		addInstruction(std::string const & fileName, long numLine, Instruction const * i);
		void		addInstruction(std::string const & fileName, long numLine, std::string & line);
		void		checkLabels(void) const;
		void		convertLabels(void);
		void		compile(void) const;
		void		bind(const char * progName) const;
		void		execute(void);

/******************************************************************************/
// Getters
/******************************************************************************/
		OperandStack						getStack(void) const;
		InstructionQueue					getInstructions(void) const;
		InstructionQueue::iterator			getCursor(void) const;
		LabelMap							getLabels(void) const;
		CarryFlag							getCarry(void) const;
		std::string const & 				getReadBuffer(void) const;
		CallStack const &					getCallStack(void) const ;

	private :
/******************************************************************************/
// Private functions
/******************************************************************************/
		void		checkCallStack(void);

		void		push(void);
		void		pop(void);
		void		dump(void);
		void		assert(void);
		void		add(void);
		void		sub(void);
		void		mul(void);
		void		div(void);
		void		mod(void);
		void		print(void);
		void		exit(void);

/******************************************************************************/
// Additional private functions (bonus)
/******************************************************************************/
		void		putn(void);
		void		put(void);
		void		swap(void);
		void		dup(void);
		void		read(void);

		void		cmp(void);
		void		jmp(void);
		void		call(void);

		void		je(void);
		void		jne(void);
		void		jlt(void);
		void		jgt(void);
		void		jle(void);
		void		jge(void);

		void		jc(void);
		void		jnc(void);
		void		jse(void);
		void		jso(void);
		void		js(void);
		void		jeof(void);
	};
}

#endif /* !ENGINE_HPP */
