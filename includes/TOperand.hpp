#ifndef T_OPERAND_HPP
# define T_OPERAND_HPP

# include "IOperand.hpp"
# include "OperandFactory.hpp"
# include "Instruction.hpp"
# include "TOperations.hpp"

namespace AVM
{
	template <typename T>
	class TOperand : public IOperand
	{
	private :
/******************************************************************************/
// Attributes
/******************************************************************************/
		eOperandType		_type;
		std::string			_content;
	
/******************************************************************************/
// Private default constructor
/******************************************************************************/
		TOperand() : _type(Int8), _content("0") {  } // Coplien

	public :
/******************************************************************************/
// Coplien
/******************************************************************************/
		virtual ~TOperand() {  }
		TOperand(TOperand const & ref) { *this = ref; }
		TOperand &			operator=(TOperand const & rhs)
		{
			this->_type = rhs.getType();
			this->_content = rhs.toString();
		}

/******************************************************************************/
// Constructor
/******************************************************************************/
		TOperand(eOperandType type, std::string const & content)
			: _type(type), _content(content) {  }

/******************************************************************************/
// Getters
/******************************************************************************/
		eOperandType		getType(void) const { return this->_type; }
		int					getPrecision(void) const { return static_cast<int>(this->_type); }
		std::string	const &	toString(void) const { return this->_content; }

/******************************************************************************/
// Operations
/******************************************************************************/
		IOperand const *	operator+( IOperand const & rhs ) const
		{
			typedef std::string (*addFunc)(std::string const &, std::string const &);

			OperandFactory	factory;
			addFunc			add[nbOperandType];

			add[Int8] = &Operations::add<int8_t>;
			add[Int16] = &Operations::add<int16_t>;
			add[Int32] = &Operations::add<int32_t>;
			add[Float] = &Operations::add<float>;
			add[Double] = &Operations::add<double>;

			if (this->getPrecision() >= rhs.getPrecision())
				return factory.createOperand(this->_type, Operations::add<T>(this->toString(), rhs.toString()));
			else
				return factory.createOperand(rhs.getType(), add[rhs.getType()](this->toString(), rhs.toString()));
		}


		IOperand const *	operator-( IOperand const & rhs ) const
		{
			typedef std::string (*subFunc)(std::string const &, std::string const &);

			OperandFactory	factory;
			subFunc			sub[nbOperandType];

			sub[Int8] = &Operations::sub<int8_t>;
			sub[Int16] = &Operations::sub<int16_t>;
			sub[Int32] = &Operations::sub<int32_t>;
			sub[Float] = &Operations::sub<float>;
			sub[Double] = &Operations::sub<double>;

			if (this->getPrecision() >= rhs.getPrecision())
				return factory.createOperand(this->_type, Operations::sub<T>(this->toString(), rhs.toString()));
			else
				return factory.createOperand(rhs.getType(), sub[rhs.getType()](this->toString(), rhs.toString()));
		}

		IOperand const *	operator*( IOperand const & rhs ) const
		{
			typedef std::string (*mulFunc)(std::string const &, std::string const &);

			OperandFactory	factory;
			mulFunc			mul[nbOperandType];

			mul[Int8] = &Operations::mul<int8_t>;
			mul[Int16] = &Operations::mul<int16_t>;
			mul[Int32] = &Operations::mul<int32_t>;
			mul[Float] = &Operations::mul<float>;
			mul[Double] = &Operations::mul<double>;

			if (this->getPrecision() >= rhs.getPrecision())
				return factory.createOperand(this->_type, Operations::mul<T>(this->toString(), rhs.toString()));
			else
				return factory.createOperand(rhs.getType(), mul[rhs.getType()](this->toString(), rhs.toString()));
		}

		IOperand const *	operator/( IOperand const & rhs ) const
		{
			typedef std::string (*divFunc)(std::string const &, std::string const &);

			OperandFactory	factory;
			divFunc			div[nbOperandType];

			div[Int8] = &Operations::div<int8_t>;
			div[Int16] = &Operations::div<int16_t>;
			div[Int32] = &Operations::div<int32_t>;
			div[Float] = &Operations::div<float>;
			div[Double] = &Operations::div<double>;

			if (this->getPrecision() >= rhs.getPrecision())
				return factory.createOperand(this->_type, Operations::div<T>(this->toString(), rhs.toString()));
			else
				return factory.createOperand(rhs.getType(), div[rhs.getType()](this->toString(), rhs.toString()));
		}

		IOperand const *	operator%( IOperand const & rhs ) const
		{
			typedef std::string (*modFunc)(std::string const &, std::string const &);

			OperandFactory	factory;
			modFunc			mod[nbOperandType];

			mod[Int8] = &Operations::mod<int8_t>;
			mod[Int16] = &Operations::mod<int16_t>;
			mod[Int32] = &Operations::mod<int32_t>;
			mod[Float] = &Operations::mod<float>;
			mod[Double] = &Operations::mod<double>;

			if (this->getPrecision() >= rhs.getPrecision())
				return factory.createOperand(this->getType(), mod[this->getType()](this->toString(), rhs.toString()));
			else
				return factory.createOperand(rhs.getType(), mod[rhs.getType()](this->toString(), rhs.toString()));
		}
	};
}

#endif
