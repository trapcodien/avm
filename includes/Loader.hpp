#ifndef LOADER_HPP
# define LOADER_HPP

# include <fstream>
# include "Engine.hpp"

namespace AVM
{
	namespace Loader
	{
		void		loadInstructions(Engine * vm, std::basic_istream<char> & is, char const * fileName);
		void		loadFile(Engine *vm, char const * fileName);
	}
}

#endif /* !LOADER_HPP */
