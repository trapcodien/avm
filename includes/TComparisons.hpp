#ifndef T_COMPARISONS_HPP
# define T_COMPARISONS_HPP

# include <cmath>
# include <sstream>
# include <iomanip>
# include <limits>
# include "RunTimeException.hpp"

namespace AVM
{
	namespace Comparisons
	{
/*****************************************************************************/
// strToType()
/*****************************************************************************/
		template <typename T>
		T				strToType(std::string const & str)
		{
			T 					result;
			std::stringstream	ss;

			ss << str;
			if (!(ss >> result))
				throw (RunTimeException(RunTimeException::BadValue, 0, str));
			return result;
		}

	// int8_t
		template <>
		int8_t			strToType(std::string const & str)
		{
			int16_t				result;
			std::stringstream	ss;

			ss << str;
			if (!(ss >> result))
				throw (RunTimeException(RunTimeException::BadValue, 0, str));
			if (result < std::numeric_limits<int8_t>::min() || result > std::numeric_limits<int8_t>::max())
				throw (RunTimeException(RunTimeException::BadValue, 0, str));
			return static_cast<int8_t>(result);
		}

/*****************************************************************************/
// cmp_lt()
/*****************************************************************************/
		template <typename T>
		bool			lt(std::string const & lhs, std::string const & rhs)
		{
			T	a = strToType<T>(lhs);
			T	b = strToType<T>(rhs);
			if (a < b)
				return true;
			return false;
		}

/*****************************************************************************/
// cmp_gt()
/*****************************************************************************/
		template <typename T>
		bool			gt(std::string const & lhs, std::string const & rhs)
		{
			T	a = strToType<T>(lhs);
			T	b = strToType<T>(rhs);
			if (a > b)
				return true;
			return false;
		}
	}
}

#endif /* !T_COMPARISONS_HPP */
