#ifndef OPERAND_TYPE_HPP
# define OPERAND_TYPE_HPP

namespace AVM
{
	enum eOperandType { Int8 = 0, Int16, Int32, Float, Double, nbOperandType };
}

#endif /* !OPERAND_TYPE_HPP */
