#ifndef T_OPERATIONS_HPP
# define T_OPERATIONS_HPP

# include <stdint.h>
# include <cmath>
# include <sstream>
# include <iomanip>
# include <limits>
# include "RunTimeException.hpp"

namespace AVM
{
	namespace Operations
	{
/******************************************************************************/
// strToType()     
/******************************************************************************/
		template <typename T>
		T				strToType(std::string const & str)
		{
			T 					result;
			std::stringstream	ss;

			ss << str;
			if (!(ss >> result))
				throw (RunTimeException(RunTimeException::BadValue, 0, str));
			return result;
		}

	// int8_t
		template <>
		int8_t			strToType(std::string const & str)
		{
			int16_t				result;
			std::stringstream	ss;

			ss << str;
			if (!(ss >> result))
				throw (RunTimeException(RunTimeException::BadValue, 0, str));
			if (result < std::numeric_limits<int8_t>::min() || result > std::numeric_limits<int8_t>::max())
				throw (RunTimeException(RunTimeException::BadValue, 0, str));
			return static_cast<int8_t>(result);
		}

/******************************************************************************/
// toStr()     
/******************************************************************************/
		template <typename T>
		std::string		toStr(T value)
		{
			std::string			result;
			std::stringstream	ss;

			ss << std::setprecision(15) << value;
			ss >> result;
			return result;
		}

	// int8_t
		template <>
		std::string		toStr(int8_t value)
		{
			std::string			result;
			std::stringstream	ss;

			ss << static_cast<int16_t>(value);
			ss >> result;
			return result;
		}

/******************************************************************************/
// add()     
/******************************************************************************/
		template <typename T>
		std::string		add(std::string const & lhs, std::string const & rhs)
		{
			T	a = strToType<T>(lhs);
			T	b = strToType<T>(rhs);
			T	result;

				// Overflow
			if (a > 0 && b > 0 && a > (std::numeric_limits<T>::max() - b))
				throw (RunTimeException(RunTimeException::Overflow, 0, "add fail"));
				// Underflow
			if (a < 0 && b < 0 && a < (std::numeric_limits<T>::min() - b))
				throw (RunTimeException(RunTimeException::Underflow, 0, "add fail"));

			result = a + b;
			return toStr<T>(result);
		}

	// float
		template <>
		std::string		add<float>(std::string const & lhs, std::string const & rhs)
		{
			float a = strToType<float>(lhs);
			float b = strToType<float>(rhs);
			float result;

			result = a + b;

				// Overflow
			if (result == std::numeric_limits<float>::infinity())
				throw (RunTimeException(RunTimeException::Overflow, 0, "add fail - Float violation"));
				// Underflow
			if (result == -std::numeric_limits<float>::infinity())
				throw (RunTimeException(RunTimeException::Underflow, 0, "add fail - Float violation"));

			return toStr<float>(result);
		}

	// double
		template <>
		std::string		add<double>(std::string const & lhs, std::string const & rhs)
		{
			double a = strToType<double>(lhs);
			double b = strToType<double>(rhs);
			double result;

			result = a + b;

				// Overflow
			if (result == std::numeric_limits<double>::infinity())
				throw (RunTimeException(RunTimeException::Overflow, 0, "add fail - Double violation"));
				// Underflow
			if (result == -std::numeric_limits<double>::infinity())
				throw (RunTimeException(RunTimeException::Underflow, 0, "add fail - Double violation"));

			return toStr<double>(result);
		}

/******************************************************************************/
// sub()     
/******************************************************************************/
		template <typename T>
		std::string		sub(std::string const & lhs, std::string const & rhs)
		{
			T	a = strToType<T>(lhs);
			T	b = strToType<T>(rhs);
			T	result;

				// Overflow
			if (a > 0 && b < 0 && a > (std::numeric_limits<T>::max() + b))
				throw (RunTimeException(RunTimeException::Overflow, 0, "sub fail"));
				// Underflow
			if (a < 0 && b > 0 && a < (std::numeric_limits<T>::min() + b))
				throw (RunTimeException(RunTimeException::Underflow, 0, "sub fail"));

			result = a - b;
			return toStr<T>(result);
		}

	// float
		template <>
		std::string		sub<float>(std::string const & lhs, std::string const & rhs)
		{
			float a = strToType<float>(lhs);
			float b = strToType<float>(rhs);
			float result;

			result = a - b;

				// Overflow
			if (result == std::numeric_limits<float>::infinity())
				throw (RunTimeException(RunTimeException::Overflow, 0, "sub fail - Float violation"));
				// Underflow
			if (result == -std::numeric_limits<float>::infinity())
				throw (RunTimeException(RunTimeException::Underflow, 0, "sub fail - Float violation"));

			return toStr<float>(result);
		}

	// double
		template <>
		std::string		sub<double>(std::string const & lhs, std::string const & rhs)
		{
			double a = strToType<double>(lhs);
			double b = strToType<double>(rhs);
			double result;

			result = a - b;

				// Overflow
			if (result == std::numeric_limits<double>::infinity())
				throw (RunTimeException(RunTimeException::Overflow, 0, "sub fail - Double violation"));
				// Underflow
			if (result == -std::numeric_limits<double>::infinity())
				throw (RunTimeException(RunTimeException::Underflow, 0, "sub fail - Double violation"));

			return toStr<double>(result);
		}


/******************************************************************************/
// mul()     
/******************************************************************************/
		template <typename T>
		std::string		mul(std::string const & lhs, std::string const & rhs)
		{
			T	a = strToType<T>(lhs);
			T	b = strToType<T>(rhs);
			T	result;

				// Overflow
			if (a > 0 && b > 0 && a > std::numeric_limits<T>::max() / b)
				throw (RunTimeException(RunTimeException::Overflow, 0, "mul fail"));
			if (a < 0 && b < 0 && a < std::numeric_limits<T>::max() / b)
				throw (RunTimeException(RunTimeException::Overflow, 0, "mul fail"));

				// Underflow
			if (a > 0 && b < 0 && a > std::numeric_limits<T>::min() / b )
				throw (RunTimeException(RunTimeException::Underflow, 0, "mul fail"));
			if (a < 0 && b > 0 && a < std::numeric_limits<T>::min() / b)
				throw (RunTimeException(RunTimeException::Underflow, 0, "mul fail"));

			result = a * b;
			return toStr<T>(result);
		}

	// float
		template <>
		std::string		mul<float>(std::string const & lhs, std::string const & rhs)
		{
			float a = strToType<float>(lhs);
			float b = strToType<float>(rhs);
			float result;

			result = a * b;

				// Overflow
			if (result == std::numeric_limits<float>::infinity())
				throw (RunTimeException(RunTimeException::Overflow, 0, "mul fail - Float violation"));
				// Underflow
			if (result == -std::numeric_limits<float>::infinity())
				throw (RunTimeException(RunTimeException::Underflow, 0, "mul fail - Float violation"));

			return toStr<float>(result);
		}

	// double
		template <>
		std::string		mul<double>(std::string const & lhs, std::string const & rhs)
		{
			double a = strToType<double>(lhs);
			double b = strToType<double>(rhs);
			double result;

			result = a * b;

				// Overflow
			if (result == std::numeric_limits<double>::infinity())
				throw (RunTimeException(RunTimeException::Overflow, 0, "mul fail - Double violation"));
				// Underflow
			if (result == -std::numeric_limits<double>::infinity())
				throw (RunTimeException(RunTimeException::Underflow, 0, "mul fail - Double violation"));

			return toStr<double>(result);
		}

/******************************************************************************/
// div()     
/******************************************************************************/
		template <typename T>
		std::string		div(std::string const & lhs, std::string const & rhs)
		{
			T 	left = strToType<T>(lhs);
			T 	right = strToType<T>(rhs);

				// (-min / -1) case
			if (left == static_cast<T>(std::numeric_limits<T>::min()) && right == static_cast<T>(-1))
				throw (RunTimeException(RunTimeException::Overflow, 0, "div fail"));

				// Division by 0
			if (right == static_cast<T>(0))
				throw (RunTimeException(RunTimeException::FloatingPoint, 0, "Division by 0"));

			return toStr<T>(left / right);
		}

	// float
		template <>
		std::string		div<float>(std::string const & lhs, std::string const & rhs)
		{
			float a = strToType<float>(lhs);
			float b = strToType<float>(rhs);
			float result;

			result = a / b;

				// Overflow
			if (result == std::numeric_limits<float>::infinity())
				throw (RunTimeException(RunTimeException::Overflow, 0, "div fail - Float violation"));
				// Underflow
			if (result == -std::numeric_limits<float>::infinity())
				throw (RunTimeException(RunTimeException::Underflow, 0, "div fail - Float violation"));

				// Division by 0
			if (b == static_cast<float>(0))
				throw (RunTimeException(RunTimeException::FloatingPoint, 0, "Division by 0"));

			return toStr<float>(result);
		}

	// double
		template <>
		std::string		div<double>(std::string const & lhs, std::string const & rhs)
		{
			double a = strToType<double>(lhs);
			double b = strToType<double>(rhs);
			double result;

			result = a / b;

				// Overflow
			if (result == std::numeric_limits<double>::infinity())
				throw (RunTimeException(RunTimeException::Overflow, 0, "div fail - Double violation"));
				// Underflow
			if (result == -std::numeric_limits<double>::infinity())
				throw (RunTimeException(RunTimeException::Underflow, 0, "div fail - Double violation"));

				// Division by 0
			if (b == static_cast<double>(0))
				throw (RunTimeException(RunTimeException::FloatingPoint, 0, "Division by 0"));

			return toStr<double>(result);
		}


/******************************************************************************/
// mod()     
/******************************************************************************/
		template <typename T>
		std::string		mod(std::string const & lhs, std::string const & rhs)
		{
			T	right = strToType<T>(rhs);

				// Modulo by 0
			if (right == static_cast<T>(0))
				throw (RunTimeException(RunTimeException::FloatingPoint, 0, "Modulo by 0"));

			return toStr<T>(strToType<T>(lhs) % strToType<T>(rhs));
		}

	// float
		template <>
		std::string		mod<float>(std::string const &lhs, std::string const & rhs)
		{
			float		a = strToType<float>(lhs);
			float		b = strToType<float>(rhs);

				// Modulo by 0
			if (b == static_cast<float>(0))
				throw (RunTimeException(RunTimeException::FloatingPoint, 0, "Modulo by 0"));

			return toStr<float>(fmod(a, b));
		}

	// double
		template <>
		std::string		mod<double>(std::string const &lhs, std::string const & rhs)
		{
			double		a = strToType<double>(lhs);
			double		b = strToType<double>(rhs);

				// Modulo by 0
			if (b == static_cast<double>(0))
				throw (RunTimeException(RunTimeException::FloatingPoint, 0, "Modulo by 0"));

			return toStr<double>(fmod(a, b));
		}
	}
}

#endif /* !T_OPERATIONS_HPP */
