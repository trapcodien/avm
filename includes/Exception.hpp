#ifndef EXCEPTION_HPP
# define EXCEPTION_HPP

# include <iostream>

namespace AVM
{
	class Exception : public std::exception
	{
	public :
		enum type { 	FatalError = 0, FileError, FileWriteError, BadFormat, 
						ParseError, UnknowInstruction,
						BadValue, Overflow, Underflow, 
						EmptyStack, FloatingPoint, 
						NoExit, Assert, InsufficientOperands,
						DuplicateLabel, UnknowLabel, nbType };

	protected :
		type			_error;
		std::string		_fileName;
		long			_line;
		std::string		_msg;
		std::string		_msgDetails;
		std::string		_what;

/******************************************************************************/
// Private initialization
/******************************************************************************/
		void			_init(void);

	public :
/******************************************************************************/
// Coplien
/******************************************************************************/
		Exception();
		~Exception() throw();
		Exception(Exception const & ref);
		Exception &				operator=(Exception const & rhs);

/******************************************************************************/
// Constructor
/******************************************************************************/
		Exception(type error, long line = 0, std::string const & msgDetails = "");

/******************************************************************************/
// Getters
/******************************************************************************/
		type					getError(void) const;
		std::string const & 	getFileName(void) const;
		long					getLine(void) const;
		std::string const & 	getMsg(void) const;
		std::string const & 	getMsgDetails(void) const;
		std::string const & 	getWhat(void) const;

/******************************************************************************/
// Setter
/******************************************************************************/
		void					setFileName(std::string const & fileName);
		void					setLine(long line);

/******************************************************************************/
// What Overload
/******************************************************************************/
		const char *			what(void) const throw();
	};
}

#endif /* !EXCEPTION_HPP */
