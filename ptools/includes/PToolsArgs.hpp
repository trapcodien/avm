#ifndef ARGS_HPP
# define ARGS_HPP

# include <iostream>
# include <vector>
# include <map>

namespace PTools
{
	class Args
	{
	public :
/******************************************************************************/
// Typedefs
/******************************************************************************/
		typedef std::vector<std::string>	List;

/******************************************************************************/
// Exception
/******************************************************************************/
		class Exception : public std::exception
		{
		private :
			std::string		_what;

		public :
			enum type { UnknowOption = 0, NoPattern, NoArgs };

			Exception(type type);

			Exception();
			~Exception() throw();
			Exception(Exception const & ref);
			Exception &		operator=(Exception const & rhs);

			const char *	what(void) const throw();
		};

/******************************************************************************/
// Attributes
/******************************************************************************/
	private :
		std::string							_pattern;
		int									_argcPos;
		int									_argvPos;
		int									_argc;
		char **								_argv;
		List								_args;
		std::map<char, List>				_opts;
		std::map<std::string, char>			_aliases;

/******************************************************************************/
// Private functions
/******************************************************************************/
		bool								isValidOpt(char opt) const;
		int									getNbArgs(char opt) const;
		std::string							addAlias(void);
		std::string							addOpts(void);
		void								addArg(void);
		bool								operator[](std::string const & alias) const;

	public :
/******************************************************************************/
// Coplien
/******************************************************************************/
		Args();
		~Args();
		Args(Args const & ref);
		Args &		operator=(Args const & rhs);
		
/******************************************************************************/
// Constuctors
/******************************************************************************/
		Args(char const * pattern);
		Args(int argc, char **argv);
		Args(int argc, char **argv, char const * pattern);

/******************************************************************************/
// Getters
/******************************************************************************/
		std::string const &					getPattern(void) const;
		size_t								getArgcPos(void) const;
		size_t								getArgvPos(void) const;
		int									getArgc(void) const;
		char **								getArgv(void) const;
		std::map<char, List> const &		getOpts(void) const;
		std::map<std::string, char> const &	getAliases(void) const;
		List const &						getArgs(void) const;

/******************************************************************************/
// Accessor
/******************************************************************************/
			/* For get general args */
		List &								args(void); 

			/* For get specific args */
		List &								args(char opt);
		List &								args(char const * alias);
		List &								args(std::string const & alias);

/******************************************************************************/
// Main functions
/******************************************************************************/
		bool								operator[](char opt) const;

		void								setAlias(char opt, char const * alias);
		void								setAlias(char opt, std::string const & alias);

		std::string							parse(void);
		std::string							parse(char const * pattern);
		std::string							parse(int argc, char **argv);
		std::string							parse(int argc, char **argv, char const * pattern);
	};
}

#endif /* !ARGS_HPP */
