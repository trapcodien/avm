#include "PToolsArgs.hpp"

namespace PTools
{
/******************************************************************************/
// Args::Exception
/******************************************************************************/
	Args::Exception::Exception(Args::Exception::type type)
	{
		if (type == Args::Exception::UnknowOption)
			this->_what = "Unknow option.";
		else if (type == Args::Exception::NoPattern)
			this->_what = "Pattern missing.";
		else if (type == Args::Exception::NoArgs)
			this->_what = "argc/argv missing.";
	}

	Args::Exception::Exception() : std::exception() {  }
	Args::Exception::~Exception() throw() {  }

	Args::Exception::Exception(Args::Exception const & ref) : std::exception()
	{
		*this = ref;
	}

	Args::Exception &	Args::Exception::operator=(Args::Exception const & rhs)
	{
		this->_what = rhs.what();
		return *this;
	}
	
	const char *	Args::Exception::what(void) const throw()
	{
		return this->_what.c_str();
	}

/******************************************************************************/
// Private functions
/******************************************************************************/
	bool								Args::isValidOpt(char opt) const
	{
		for (size_t i = 0 ; i < this->_pattern.size() ; i++)
		{
			if (this->_pattern[i] == opt)
				return true;
		}
		return false;
	}

	int									Args::getNbArgs(char opt) const
	{
		int		ret = 0;
		bool	readyToCount = false;

		for (size_t i = 0 ; i < this->_pattern.size() ; i++)
		{
			if (this->_pattern[i] == opt)
				readyToCount = true;
			else if (readyToCount)
			{
				if (this->_pattern[i] == ':')
					ret++;
				else
					return ret;
			}
		}
		return ret;
	}

	std::string							Args::addAlias(void)
	{
		std::string alias(&this->_argv[this->_argcPos][2]);

		if (!((*this)[alias]))
			return alias;
		
		char opt = this->_aliases[alias];
		if (!isValidOpt(opt))
			return alias;
		this->_opts[opt];
		
		int	nbArgs = getNbArgs(opt);
		for (int i = 1 ; i <= nbArgs && this->_argcPos + i < this->_argc ; i++)
			this->_opts[opt].push_back(std::string(this->_argv[this->_argcPos + i]));

		this->_argcPos += nbArgs;
		return std::string("");
	}

	std::string							Args::addOpts(void)
	{
		std::string opts(this->_argv[this->_argcPos]);
		std::string ret(" ");
		char opt = 0;

		for (size_t i = 1 ; i < opts.size() ; i++)
		{
			opt = opts[i];
			if (!isValidOpt(opt))
			{
				ret[0] = opt;
				return ret;
			}
			this->_opts[opt];
		}
		if (!opt)
			return opts;

		int nbArgs = getNbArgs(opt);
		for (int i = 1 ; i <= nbArgs && this->_argcPos + i < this->_argc ; i++)
			this->_opts[opt].push_back(std::string(this->_argv[this->_argcPos + i]));

		this->_argcPos += nbArgs;
		return std::string("");
	}

	void									Args::addArg(void)
	{
		if (this->_argcPos < this->_argc)
			this->_args.push_back(std::string(this->_argv[this->_argcPos]));
	}

	bool								Args::operator[](std::string const & alias) const
	{
		if (this->_aliases.find(alias) == this->_aliases.end())
			return false;
		return true;
	}

/******************************************************************************/
// Coplien
/******************************************************************************/
	Args::Args() : _argcPos(1), _argvPos(0), _argv(NULL) {  }
	Args::~Args() {  }
	Args::Args(Args const & ref) { *this = ref; }

	Args &								Args::operator=(Args const & rhs)
	{
		this->_pattern = rhs.getPattern();
		this->_argcPos = rhs.getArgcPos();
		this->_argvPos = rhs.getArgvPos();
		this->_argc = rhs.getArgc();
		this->_argv = rhs.getArgv();
		this->_opts = rhs.getOpts();
		this->_aliases = rhs.getAliases();
		this->_args = rhs.getArgs();
		return (*this);
	}

/******************************************************************************/
// Constuctors
/******************************************************************************/
	Args::Args(char const * pattern)
	: _pattern(pattern), _argcPos(1), _argvPos(0), _argv(NULL) {  }

	Args::Args(int argc, char **argv)
	: _argcPos(1), _argvPos(0), _argc(argc), _argv(argv) {  }

	Args::Args(int argc, char **argv, char const * pattern)
	: _pattern(pattern), _argcPos(1), _argvPos(0), _argc(argc), _argv(argv) {  }

/******************************************************************************/
// Getters
/******************************************************************************/
	std::string const &					Args::getPattern(void) const { return this->_pattern; }
	size_t								Args::getArgcPos(void) const { return this->_argcPos; }
	size_t								Args::getArgvPos(void) const { return this->_argvPos; }
	int									Args::getArgc(void) const { return this->_argc; }
	char **								Args::getArgv(void) const { return this->_argv; }
	std::map<char, Args::List> const &	Args::getOpts(void) const { return this->_opts; }
	std::map<std::string, char> const &	Args::getAliases(void) const { return this->_aliases; }
	Args::List const &					Args::getArgs(void) const { return this->_args; }
	
/******************************************************************************/
// Accessor
/******************************************************************************/
		// For get general args
	Args::List &						Args::args(void) { return this->_args; }

		// For get specific args
	Args::List &						Args::args(char opt)
	{
		Args & 		that = *this;

		if (!(that[opt]))
			throw Args::Exception(Args::Exception::UnknowOption);
		return this->_opts[opt];
	}

	Args::List &						Args::args(char const * alias)
	{
		Args & 		that = *this;

		if (!(that[alias]))
			throw Args::Exception(Args::Exception::UnknowOption);
		return this->args(this->_aliases[std::string(alias)]);
	}

	Args::List &						Args::args(std::string const & alias)
	{
		Args & 		that = *this;

		if (!(that[alias]))
			throw Args::Exception(Args::Exception::UnknowOption);
		return this->args(this->_aliases[alias]);
	}

/******************************************************************************/
// Main functions
/******************************************************************************/
	bool								Args::operator[](char opt) const
	{
		if (this->_opts.find(opt) == this->_opts.end())
			return false;
		return true;
	}

	void								Args::setAlias(char opt, char const * alias)
	{
		this->_aliases[std::string(alias)] = opt;
	}

	void								Args::setAlias(char opt, std::string const & alias)
	{
		this->_aliases[std::string(alias)] = opt;
	}

	std::string							Args::parse(void)
	{
		if (this->_argv == NULL)
			throw Args::Exception(Args::Exception::NoArgs);
		if (this->_pattern == "")
			throw Args::Exception(Args::Exception::NoPattern);


		if (this->_argcPos >= this->_argc && this->_argcPos == 1)
			return std::string("");
		else if (this->_argcPos == this->_argc)
		{
			this->_argcPos = 1;
			this->_argvPos = 0;
			return this->parse();
		}

		bool getOpt = true;
		std::string	ret("");

		while (this->_argcPos < this->_argc)
		{
			if (getOpt)
			{
				std::string arg(this->_argv[this->_argcPos]);
				if (arg == "--")
					getOpt = false;
				else if (arg.size() > 2 && arg[0] == '-' && arg[1] == '-')
				{
					if ((ret = this->addAlias()) != "")
						return ret;
				}
				else if (arg.size() > 1 && arg[0] == '-')
				{
					if ((ret = this->addOpts()) != "")
						return ret;
				}
				else
					this->addArg();
			}
			else
				this->addArg();
			this->_argcPos++;
		}

		return ret;
	}

	std::string							Args::parse(char const * pattern)
	{
		this->_pattern = std::string(pattern);
		return this->parse();
	}

	std::string							Args::parse(int argc, char ** argv)
	{
		this->_argc = argc;
		this->_argv = argv;
		return this->parse();
	}

	std::string							Args::parse(int argc, char **argv, char const * pattern)
	{
		this->_argc = argc;
		this->_argv = argv;
		this->_pattern = std::string(pattern);
		return this->parse();
	}
}
