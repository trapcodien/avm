#include "Parser.hpp"

namespace AVM
{
/******************************************************************************/
// Utils     
/******************************************************************************/

	std::string					Parser::trim(std::string const & str, char c)
	{
		size_t					first = str.find_first_not_of(c);
		size_t					last = str.find_last_not_of(c);
		return str.substr(first, last - first + 1);
	}

	std::string					Parser::extractRawValue(std::string const & value)
	{
		size_t					begin = value.find('(') + 1;
		size_t					end = value.find(')');
		size_t					size = end - begin;

		if (!size)
			throw (Exception(Exception::ParseError, 0, value));
		return (value.substr(begin, size));
	}

	void						Parser::cleanTabulation(std::string & line)
	{
		for (size_t i = 0 ; i < line.size() ; i++)
		{
			if (line[i] == '\t')
				line[i] = ' ';
		}
	}

/******************************************************************************/
// Checks     
/******************************************************************************/
	void						Parser::checkInt8(std::string const & raw)
	{
		int16_t					val;   // Here, we use int16_t, because stringstream understand int8_t like char. 

		if (!(std::stringstream(raw) >> val))
			throw (RunTimeException(RunTimeException::BadValue, 0, raw));
		if (val < std::numeric_limits<int8_t>::min() || val > std::numeric_limits<int8_t>::max())
			throw (RunTimeException(RunTimeException::BadValue, 0, raw));
	}

	void						Parser::checkInt16(std::string const & raw)
	{
		int16_t					val;

		if (!(std::stringstream(raw) >> val))
			throw (RunTimeException(RunTimeException::BadValue, 0, raw));
	}

	void						Parser::checkInt32(std::string const & raw)
	{
		int32_t					val;

		if (!(std::stringstream(raw) >> val))
			throw (RunTimeException(RunTimeException::BadValue, 0, raw));
	}

	void						Parser::checkFloat(std::string const & raw)
	{
		float					val;

		if (!(std::stringstream(raw) >> val))
			throw (RunTimeException(RunTimeException::BadValue, 0, raw));
	}

	void						Parser::checkDouble(std::string const & raw)
	{
		double					val;

		if (!(std::stringstream(raw) >> val))
			throw (RunTimeException(RunTimeException::BadValue, 0, raw));
	}


/******************************************************************************/
// Main parsing functions     
/******************************************************************************/
	Instruction::type			Parser::getInstructionType(std::string const & cmd)
	{
		std::map<std::string, Instruction::type>	mnemonics;

		mnemonics["push"] 	= Instruction::Push;
		mnemonics["pop"] 	= Instruction::Pop;
		mnemonics["dump"] 	= Instruction::Dump;
		mnemonics["assert"] = Instruction::Assert;
		mnemonics["add"] 	= Instruction::Add;
		mnemonics["sub"] 	= Instruction::Sub;
		mnemonics["mul"] 	= Instruction::Mul;
		mnemonics["div"] 	= Instruction::Div;
		mnemonics["mod"] 	= Instruction::Mod;
		mnemonics["print"] 	= Instruction::Print;
		mnemonics["exit"] 	= Instruction::Exit;


		mnemonics["putn"]	= Instruction::Putn;
		mnemonics["put"]	= Instruction::Put;
		mnemonics["swap"]	= Instruction::Swap;
		mnemonics["dup"]	= Instruction::Dup;
		mnemonics["read"]	= Instruction::Read;
		mnemonics["cmp"]	= Instruction::Cmp;

		mnemonics["jmp"]	= Instruction::Jmp;
		mnemonics["goto"]	= Instruction::Jmp; // goto is an alias to jmp

		mnemonics["call"]	= Instruction::Call;

		mnemonics["je"]		= Instruction::Je;
		mnemonics["jne"]	= Instruction::Jne;
		mnemonics["jlt"]	= Instruction::Jlt;
		mnemonics["jgt"]	= Instruction::Jgt;
		mnemonics["jle"]	= Instruction::Jle;
		mnemonics["jge"]	= Instruction::Jge;

		mnemonics["jc"]		= Instruction::Jc;
		mnemonics["jnc"]	= Instruction::Jnc;
		mnemonics["jse"]	= Instruction::Jse;
		mnemonics["jso"]	= Instruction::Jso;
		mnemonics["js"]		= Instruction::Js;
		mnemonics["jeof"]	= Instruction::Jeof;

		if (mnemonics.count(cmd))
			return mnemonics[cmd];

		throw (Exception(Exception::UnknowInstruction, 0, cmd));
	}

	eOperandType				Parser::getOperandType(std::string const & value)
	{
		if (*value.rbegin() == ')')
		{
			if (value.substr(0, 5) == "int8(")
				return Int8;
			if (value.substr(0, 6) == "int16(")
				return Int16;
			if (value.substr(0, 6) == "int32(")
				return Int32;
			if (value.substr(0, 6) == "float(")
				return Float;
			if (value.substr(0, 7) == "double(")
				return Double;
		}

		throw (Exception(Exception::ParseError, 0, value));
	}

	std::string					Parser::getInteger(std::string const & value)
	{
		std::string rawValue = extractRawValue(value);

		for (size_t i = 0 ; i < rawValue.size() ; i++)
		{
			if (i == 0 && (rawValue[i] != '-' && !isdigit(rawValue[i])))
				throw (RunTimeException(RunTimeException::BadValue, 0, rawValue));
			else if (i != 0 && !isdigit(rawValue[i]))
				throw (RunTimeException(RunTimeException::BadValue, 0, rawValue));
		}
		return rawValue;
	}

	std::string					Parser::getFloat(std::string const & value)
	{
		std::string rawValue = extractRawValue(value);
		bool				left = false;
		bool				right = false;
		bool				point = false;

		for (size_t i = 0 ; i < rawValue.size(); i++)
		{
			if (i == 0 && rawValue[i] == '-')
				continue ;
			else if (isdigit(rawValue[i]) && !point)
				left = true;
			else if (isdigit(rawValue[i]) && point)
				right = true;
			else if (rawValue[i] == '.' && !point && left)
				point = true;
			else
				break ;
		}
		if (!left || !right || !point)
			throw (RunTimeException(RunTimeException::BadValue, 0, rawValue));
		return rawValue;
	}


	Instruction const *			Parser::createLabel(long numLine, std::string const & line)
	{
		std::string label = Parser::trim(line, ' ');
		if (label[label.size() - 1] != ':')
			return NULL;

		size_t posSpace = label.find_last_not_of(' ', label.size() - 2) + 1;
		label.erase(posSpace, label.size() - posSpace);

		if (label.find(' ') != label.npos)
			throw (Exception(Exception::ParseError, numLine, "Invalid Label Name"));

		return new Instruction(numLine, Instruction::Label, label);
	}

/******************************************************************************/
// Public parsing functions
/******************************************************************************/
	bool						Parser::isBlank(std::string const & line)
	{
		size_t	size = line.size();

		for (size_t i = 0; i < size ; i++)
		{
			if (line[i] == ';' || line[i] == '#')
				return true;
			if (line[i] != ' ' && line[i] != '\t')
				return false;
		}
		return true;
	}

	Instruction const *			Parser::createInstruction(long numLine, std::string & line)
	{
		line = line.substr(0, line.find(';', 0));
		line = line.substr(0, line.find('#', 0));
		std::transform(line.begin(), line.end(), line.begin(), ::tolower);
		cleanTabulation(line);
		Instruction const * instruction;

		if ((instruction = createLabel(numLine, line)) != NULL) // Label
			return instruction;

		std::vector<std::string>		parsedLine = Lexer::split(line, ' ');

		if (parsedLine.size() == 2) // Assert/Push
		{
			typedef void			(*check_func)(std::string const &);

			check_func				check[nbOperandType];
			Instruction::type		type;
			eOperandType			operandType;
			std::string				operandValue;

			check[Int8] = &Parser::checkInt8;
			check[Int16] = &Parser::checkInt16;
			check[Int32] = &Parser::checkInt32;
			check[Float] = &Parser::checkFloat;
			check[Double] = &Parser::checkDouble;

			type = getInstructionType(parsedLine[0]);

			if (type >= Instruction::Jmp) // Jump
				return new Instruction(numLine, type, parsedLine[1]);

			if (type != Instruction::Assert && type != Instruction::Push)
				throw (Exception(Exception::ParseError, 0, parsedLine[0]));

			operandType = getOperandType(parsedLine[1]);
			if (operandType >= Float)
				operandValue = getFloat(parsedLine[1]);
			else
				operandValue = getInteger(parsedLine[1]);

			(check[operandType])(operandValue);
			return new Instruction(numLine, type, operandValue, operandType);
		}
		else if (parsedLine.size() == 1) // Other basic instructions
		{
			if (parsedLine[0] == "assert" || parsedLine[0] == "push")
				throw (Exception(Exception::ParseError, 0, parsedLine[0]));
			return new Instruction(numLine, getInstructionType(parsedLine[0]));
		}

		throw (Exception(Exception::ParseError, 0, line));
	}	
}
