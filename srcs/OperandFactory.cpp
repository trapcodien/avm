#include "OperandFactory.hpp"
#include "TOperand.hpp"

namespace AVM
{
/******************************************************************************/
// Private
/******************************************************************************/
	IOperand const *	OperandFactory::createInt8(std::string const & value) const
	{
		IOperand const * ptr = new TOperand<int8_t>(Int8, value);
		return ptr;
	}

	IOperand const *	OperandFactory::createInt16(std::string const & value) const
	{
		IOperand const * ptr = new TOperand<int16_t>(Int16, value);
		return ptr;
	}

	IOperand const *	OperandFactory::createInt32(std::string const & value) const
	{
		IOperand const * ptr = new TOperand<int32_t>(Int32, value);
		return ptr;
	}

	IOperand const *	OperandFactory::createFloat(std::string const & value) const
	{
		IOperand const * ptr = new TOperand<float>(Float, value);
		return ptr;
	}

	IOperand const *	OperandFactory::createDouble(std::string const & value) const
	{
		IOperand const * ptr = new TOperand<double>(Double, value);
		return ptr;
	}

/******************************************************************************/
// Public
/******************************************************************************/
	IOperand const *	OperandFactory::createOperand(eOperandType type, std::string const & value) const
	{
		typedef  IOperand const * (OperandFactory::*createFunc)(std::string const & value) const;
		createFunc		creator[nbOperandType];

		creator[Int8] = &OperandFactory::createInt8;
		creator[Int16] = &OperandFactory::createInt16;
		creator[Int32] = &OperandFactory::createInt32;
		creator[Float] = &OperandFactory::createFloat;
		creator[Double] = &OperandFactory::createDouble;

		return (this->*creator[type])(value);
	}
}
