#include "Lexer.hpp"

namespace AVM
{
	std::vector<std::string>	Lexer::split(std::string const & str, char delimiter)
	{
		std::stringstream			ss(str);
		std::string					piece;
		std::vector<std::string>	ret;

		while (std::getline(ss, piece, delimiter))
		{
			if (piece != "")
				ret.push_back(piece);
		}
		return ret;
	}
}
