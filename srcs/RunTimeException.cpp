#include "RunTimeException.hpp"

namespace AVM
{
	RunTimeException::RunTimeException()
		: Exception(UnknowInstruction, -1, "")
	{
		this->_init();
	}

	RunTimeException::~RunTimeException() throw()
	{

	}

	RunTimeException::RunTimeException(RunTimeException const & ref)
		: Exception()
	{
		*this = ref;
	}

	RunTimeException &	RunTimeException::operator=(RunTimeException const & rhs)
	{
		this->_error = rhs.getError();
		this->_fileName = rhs.getFileName();
		this->_line = rhs.getLine();
		this->_msg = rhs.getMsg();
		this->_msgDetails = rhs.getMsgDetails();
		this->_what = rhs.getWhat();
		return (*this);
	}

	RunTimeException::RunTimeException(RunTimeException::type error, long line, std::string const & msgDetails)
		: Exception(static_cast<Exception::type>(error), line, msgDetails)
	{
		this->_init();
	}
}

