#include "Loader.hpp"

namespace AVM
{
	namespace Loader
	{
		void		loadInstructions(Engine * vm, std::basic_istream<char> & is, char const * fileName)
		{
			long			magic = Engine::rMagicBytecode;
			long			line = 1;
			std::string		readedLine;
			bool			first = true;

			if (std::getline(is, readedLine)
					&& (magic == *(reinterpret_cast<long const *>(readedLine.c_str()))))
			{
				Instruction const * i;

				while ((i = Instruction::interpretBytecode(is)))
					vm->addInstruction(fileName, line, i);
				return ;
			}
			while (first || std::getline(is, readedLine))
			{
				first = false;
				if (&is == &std::cin && readedLine == ";;")
					break ;
				if (!Parser::isBlank(readedLine))
					vm->addInstruction(fileName, line, readedLine);
				line++;
			}
		}

		void		loadFile(Engine *vm, char const * fileName)
		{
			std::ifstream file(fileName);

			if (!file)
				throw (Exception(Exception::FileError, 0, fileName));

			try
			{
				loadInstructions(vm, file, fileName);
			}
			catch (Exception const & e)
			{
				file.close();
				throw e;
			}

			file.close();
		}
	}
}
