#include <sstream>
#include "Exception.hpp"

namespace AVM
{
/******************************************************************************/
// Private initialization 
/******************************************************************************/
	void		Exception::_init(void)
	{
		switch (this->_error)
		{
			case FatalError:
				this->_msg = "Fatal Error" ; break;
			case FileError:
				this->_msg = "Unable to load file" ; break;
			case FileWriteError:
				this->_msg = "Unable to write file" ; break;
			case BadFormat:
				this->_msg = "Bad AVM format" ; break;
			case ParseError:
				this->_msg = "Parse error"; break;
			case UnknowInstruction:
				this->_msg = "Unknow instruction"; break;
			case BadValue:
				this->_msg = "Bad value"; break;
			case Overflow:
				this->_msg = "Overflow"; break;
			case Underflow:
				this->_msg = "Underflow"; break;
			case EmptyStack:
				this->_msg = "Stack is empty"; break;
			case FloatingPoint:
				this->_msg = "Floating Point Exception"; break;
			case NoExit:
				this->_msg = "Program doesn't have an exit instruction"; break;
			case Assert:
				this->_msg = "Assert is false"; break;
			case InsufficientOperands:
				this->_msg = "Insufficient number of operands in the stack"; break;
			case DuplicateLabel:
				this->_msg = "Duplicate Label"; break;
			case UnknowLabel:
				this->_msg = "Unknow Label"; break;
			default:
				this->_msg = "Error";	
		}

		std::stringstream ss;

		if (this->_fileName != "" && this->_line > 0)
			ss << this->_fileName << ":" << this->_line << " : ";
		else if (this->_line > 0)
			ss << "line " << this->_line << " : ";
		ss << this->_msg;
		if (this->_msgDetails != "")
			ss << " : '" << this->_msgDetails << "'";

		this->_what.clear();
		this->_what = ss.str();
	}

/******************************************************************************/
// Coplien
/******************************************************************************/
	Exception::Exception()
		: std::exception(), _error(UnknowInstruction), _line(-1)
	{
		this->_init();
	}

	Exception::~Exception() throw()
	{

	}

	Exception::Exception(Exception const & ref)
		: std::exception()
	{
		*this = ref;
	}

	Exception &	Exception::operator=(Exception const & rhs)
	{
		this->_error = rhs.getError();
		this->_fileName = rhs.getFileName();
		this->_line = rhs.getLine();
		this->_msg = rhs.getMsg();
		this->_msgDetails = rhs.getMsgDetails();
		this->_what = rhs.getWhat();
		return (*this);
	}

/******************************************************************************/
// Constructor
/******************************************************************************/
	Exception::Exception(Exception::type error, long line, std::string const & msgDetails)
		: std::exception(), _error(error), _line(line), _msgDetails(msgDetails)
	{
		this->_init();
	}

/******************************************************************************/
// Getters
/******************************************************************************/
	Exception::type		Exception::getError(void) const { return this->_error; }
	std::string const &	Exception::getFileName(void) const { return this->_fileName; }
	long				Exception::getLine(void) const { return this->_line; }
	std::string const &	Exception::getMsg(void) const { return this->_msg; }
	std::string const & Exception::getMsgDetails(void) const { return this->_msgDetails; }
	std::string const &	Exception::getWhat(void) const { return this->_what; }

/******************************************************************************/
// Setter
/******************************************************************************/
	void				Exception::setFileName(std::string const & fileName)
	{
		this->_fileName = fileName ; this->_init();
	}

	void				Exception::setLine(long line)
	{
		this->_line = line; this->_init();
	}

/******************************************************************************/
// What Overload
/******************************************************************************/
	const char *		Exception::what(void) const throw() { return &this->_what[0]; }
}
