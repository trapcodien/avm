#include "Injector.hpp"

namespace AVM
{
	namespace Injector
	{
		uint64_t	offset;

		static uint32_t	get_n_value(struct mach_header_64 * header)
		{
			char 					*ptr = reinterpret_cast<char *>(header);
			struct load_command		*loader = reinterpret_cast<struct load_command *>(header + 1);
			uint32_t				i = 0;
			
			while (i < header->ncmds)
			{
				if (loader->cmd == LC_SYMTAB)
				{
					struct symtab_command 	*symtab = reinterpret_cast<struct symtab_command *>(loader);
					struct nlist_64			*symbol = reinterpret_cast<struct nlist_64 *>(ptr + symtab->symoff);
					uint32_t				j = 0;
					while (j < symtab->nsyms)
					{
						std::string str(ptr + symtab->stroff + symbol->n_un.n_strx);

						if (str.find("avm_execution_buffer") != str.npos)
							return symbol->n_value;
						symbol++;
						j++;
					}
				}
				loader = reinterpret_cast<load_command *>((reinterpret_cast<char *>(loader) + loader->cmdsize));
				i++;
			}
			(void)header;
			return (0);
		}

		void		prepare(char const * fileName)
		{
			int						fd;
			struct stat				file_stat;
			void					*ptr;
			size_t					size;
			struct mach_header_64	*header;

			ptr = MAP_FAILED;
			size = 0;
			if ((fd = open(fileName, O_RDONLY)) == -1)
				throw Exception(Exception::FileError);
			if (fstat(fd, &file_stat) != 0)
			{
				close(fd);
				throw Exception(Exception::FileError, 0, "Unable to get stat on file");
			}
			size = static_cast<size_t>(file_stat.st_size);
			ptr = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
			if (ptr == MAP_FAILED)
				throw Exception(Exception::FatalError, 0, "mmap error");
			close(fd);

			header = reinterpret_cast<struct mach_header_64 *>(ptr);
			if (header->magic != MH_MAGIC_64 || header->filetype != MH_EXECUTE)
				throw Exception(Exception::FatalError,0 , "Bad binary file");

			uint32_t	n_value = get_n_value(header);
			offset = static_cast<uint64_t>(n_value);
			if (!n_value)
				throw Exception(Exception::FatalError, 0, "Unable to get Injection Offset");

			if (munmap(ptr, size) == -1)
				throw Exception(Exception::FatalError, 0, "munmap error");
		}
	}
}

