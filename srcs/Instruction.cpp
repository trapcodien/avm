#include "Instruction.hpp"
#include "Exception.hpp"

namespace AVM
{
/******************************************************************************/
// Private Coplien
/******************************************************************************/
	Instruction::Instruction()
	{
		throw (Exception(Exception::FatalError, 0, "Bad Instruction constructor called"));
	}

/******************************************************************************/
// Coplien
/******************************************************************************/
	Instruction::~Instruction()
	{
		
	}

	Instruction::Instruction(Instruction const & ref)
	{
		*this = ref;
	}

	Instruction &	Instruction::operator=(Instruction const & rhs)
	{
		this->_type = rhs.getType();
		this->_operandValue = rhs.getOperandValue();
		this->_operandType = rhs.getOperandType();
		this->_line = rhs.getLine();
		return (*this);
	}

/******************************************************************************/
// Constructors
/******************************************************************************/
	Instruction::Instruction(long line, type type) : _type(type), _line(line)
	{
		if (type == Assert || type == Push)
			throw (Exception(Exception::FatalError, line, "Bad Instruction constructor called"));
	}

	Instruction::Instruction(long line, type type, std::string const & value, eOperandType valueType)
	: _type(type), _operandValue(value), _operandType(valueType), _line(line)
	{
		if (type != Assert && type != Push)
			throw (Exception(Exception::FatalError, line, "Bad Instruction constructor called"));
	}

	Instruction::Instruction(long line, type type, std::string const & label)
	: _type(type), _operandValue(label), _line(line)
	{
		if (type < Label)
			throw (Exception(Exception::FatalError, line, "Bad Instruction constructor called"));
	}

/******************************************************************************/
// Getters
/******************************************************************************/
	Instruction::type	Instruction::getType(void) const { return this->_type; }
	std::string const &	Instruction::getOperandValue(void) const { return this->_operandValue; }
	eOperandType		Instruction::getOperandType(void) const { return this->_operandType; }
	long				Instruction::getLine(void) const { return this->_line; }
	std::string const &	Instruction::getFileName(void) const { return this->_fileName; }

/******************************************************************************/
// Setter
/******************************************************************************/
	void				Instruction::setFileName(std::string const & fileName)
	{
		this->_fileName = fileName;
	}

	void				Instruction::setOperandValue(unsigned int operandValue)	
	{
		std::stringstream ss;

		ss << operandValue;
		this->_operandValue.erase();
		ss >> this->_operandValue;
	}
	
/******************************************************************************/
// Serialization / Unserialization
/******************************************************************************/
	void				Instruction::operator>>(std::basic_ostream<char> & ofs) const
	{
		if (this->_type < Instruction::Pop) // Value
		{
			ofs.put(static_cast<char>(this->_type));
			ofs.put(static_cast<char>(this->_operandType));
			if (this->_operandType == Int8)
			{
				int8_t n = Operations::strToType<int8_t>(this->_operandValue);
				ofs.write(reinterpret_cast<const char *>(&n), sizeof(int8_t));
			}
			else if (this->_operandType == Int16)
			{
				int16_t n = Operations::strToType<int16_t>(this->_operandValue);
				ofs.write(reinterpret_cast<const char *>(&n), sizeof(int16_t));
			}
			else if (this->_operandType == Int32)
			{
				int32_t n = Operations::strToType<int32_t>(this->_operandValue);
				ofs.write(reinterpret_cast<const char *>(&n), sizeof(int32_t));
			}
			else if (this->_operandType == Float)
			{
				float n  = Operations::strToType<float>(this->_operandValue);
				ofs.write(reinterpret_cast<const char *>(&n), sizeof(float));
			}
			else if (this->_operandType == Double)
			{
				double n = Operations::strToType<double>(this->_operandValue);
				ofs.write(reinterpret_cast<const char *>(&n), sizeof(double));
			}
		}
		else if (this->_type >= Instruction::Pop && this->_type <= Instruction::Cmp) // Simple
			ofs.put(static_cast<char>(this->_type));
		else // Label
		{
			ofs.put(static_cast<char>(this->_type));
			int32_t n = Operations::strToType<int32_t>(this->_operandValue);
			ofs.write(reinterpret_cast<const char *>(&n), sizeof(int32_t));
		}
	}

	Instruction const *	Instruction::interpretBytecode(std::basic_istream<char> & ifs)
	{
		char			*read_ptr;
		unsigned char	instruction;
		unsigned char	type_value;
		int32_t			label;

		read_ptr = reinterpret_cast<char *>(&instruction);
		ifs.read(read_ptr, sizeof(char));
		if (ifs.gcount() != sizeof(char))
			return NULL;

		if (instruction < Instruction::Pop)
		{ // Value
			read_ptr = reinterpret_cast<char *>(&type_value);
			ifs.read(read_ptr, sizeof(char));
			if (ifs.gcount() != sizeof(char))
				throw Exception(Exception::BadFormat);
			if (type_value == Int8)
			{
				int8_t n;
				read_ptr = reinterpret_cast<char *>(&n);
				ifs.read(read_ptr, sizeof(int8_t));
				if (ifs.gcount() != sizeof(int8_t))
					throw Exception(Exception::BadFormat);
				return new Instruction(0, static_cast<type>(instruction), Operations::toStr(n), static_cast<eOperandType>(type_value));
			}
			else if (type_value == Int16)
			{
				int16_t n;
				read_ptr = reinterpret_cast<char *>(&n);
				ifs.read(read_ptr, sizeof(int16_t));
				if (ifs.gcount() != sizeof(int16_t))
					throw Exception(Exception::BadFormat);
				return new Instruction(0, static_cast<type>(instruction), Operations::toStr(n), static_cast<eOperandType>(type_value));
			}
			else if (type_value == Int32)
			{
				int32_t n;
				read_ptr = reinterpret_cast<char *>(&n);
				ifs.read(read_ptr, sizeof(int32_t));
				if (ifs.gcount() != sizeof(int32_t))
					throw Exception(Exception::BadFormat);
				return new Instruction(0, static_cast<type>(instruction), Operations::toStr(n), static_cast<eOperandType>(type_value));
			}
			else if (type_value == Float)
			{
				float n;
				read_ptr = reinterpret_cast<char *>(&n);
				ifs.read(read_ptr, sizeof(float));
				if (ifs.gcount() != sizeof(float))
					throw Exception(Exception::BadFormat);
				return new Instruction(0, static_cast<type>(instruction), Operations::toStr(n), static_cast<eOperandType>(type_value));
			}
			else if (type_value == Double)
			{
				double n;
				read_ptr = reinterpret_cast<char *>(&n);
				ifs.read(read_ptr, sizeof(double));
				if (ifs.gcount() != sizeof(double))
					throw Exception(Exception::BadFormat);
				return new Instruction(0, static_cast<type>(instruction), Operations::toStr(n), static_cast<eOperandType>(type_value));
			}
			else
				throw Exception(Exception::BadFormat);
		}
		else if (instruction >= Instruction::Pop && instruction <= Instruction::Cmp)
		{ // Simple
			return new Instruction(0, static_cast<type>(instruction));
		}
		else if (instruction >= Instruction::Label && instruction < Instruction::nbType)
		{ // Label
			read_ptr = reinterpret_cast<char *>(&label);
			ifs.read(read_ptr, sizeof(int32_t));
			if (ifs.gcount() != sizeof(int32_t))
				throw Exception(Exception::BadFormat);
			return new Instruction(0, static_cast<type>(instruction), Operations::toStr(label));
		}
		throw Exception(Exception::BadFormat);
	}
}
