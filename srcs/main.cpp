#include <fstream>
#include "PToolsArgs.hpp"
#include "Loader.hpp"
#include "Injector.hpp"

struct execBuf
{
	unsigned int		len;
	char				buffer[INJECTION_SIZE];
};

char const				avm_execution_buffer[sizeof(execBuf)] = {0};
execBuf const *			avm_execution_ptr = reinterpret_cast<execBuf const *>(&avm_execution_buffer);

namespace AVM
{
	int			exitSuccess(Engine *vm)
	{
		delete vm;
		if (Engine::verbose)
			std::cerr << COLOR_GREEN << "Exit Success." << COLOR_RESET << std::endl;
		return 0;
	}

	int			exitFailure(Engine *vm)
	{
		delete vm;
		if (Engine::verbose)
			std::cerr << COLOR_RED << "Exit Failure." << COLOR_RESET << std::endl;
		return 1;
	}

	void		usage(char const * progName)
	{
		std::cerr << COLOR_GREEN << "usage: " << COLOR_YELLOW << progName << " [-vcbh] [-o outfile] [file1 ...]" << COLOR_RESET << std::endl;
	}

	void		help(char const * progName)
	{
		usage(progName);
		std::cerr << std::endl;

		std::cerr << COLOR_GREEN << "Abstract VM help :" << std::endl << COLOR_RESET;
		std::cerr << COLOR_YELLOW << "\t-v / --verbose" << COLOR_RESET << "\t\tVerbose Mode." << std::endl;
		std::cerr << COLOR_YELLOW << "\t-c / --compile" << COLOR_RESET << "\t\tCompile AVM scripts in bytecodes (default output: avm.bytecode)." << std::endl;
		std::cerr << COLOR_YELLOW << "\t-b / --bind" << COLOR_RESET << "\t\tUse AVM binary as a stub for make a fully working executable (default output: a.out)." << std::endl;
		std::cerr << COLOR_YELLOW << "\t-h / --help" << COLOR_RESET << "\t\tDisplay this help." << std::endl;
		std::cerr << COLOR_YELLOW << "\t-o / --outfile" << COLOR_RESET << "\t\tChange the default output file." << std::endl;
	}
}

int 	main(int argc, char **argv)
{
	AVM::Engine * vm = new AVM::Engine();

	PTools::Args argParser(argc, argv, "vcbho:");
	argParser.setAlias('v', "verbose");
	argParser.setAlias('c', "compile");
	argParser.setAlias('b', "bind");
	argParser.setAlias('h', "help");
	argParser.setAlias('o', "outfile");

	if (avm_execution_ptr->len)
	{
		std::stringstream	ss;
		ss.write(avm_execution_ptr->buffer, avm_execution_ptr->len);

		try
		{
			AVM::Loader::loadInstructions(vm, ss, argv[0]);
			vm->checkLabels();
			vm->execute();
		}
		catch (AVM::Exception const & e)
		{
			std::cerr << COLOR_RED << "ERROR : " << COLOR_RESET << e.what() << std::endl;
		}
		return AVM::exitSuccess(vm);
	}

	if (argParser.parse() != "")
	{
		AVM::usage(argv[0]);
		return AVM::exitFailure(vm);
	}
	if (argParser['h'])
	{
		AVM::help(argv[0]);
		return AVM::exitSuccess(vm);
	}
	if (argParser['v'])
		AVM::Engine::verbose = true;

	if (argParser['o'] && argParser.args('o').size() == 1)
		AVM::Engine::output = argParser.args('o').front();

	try
	{
		PTools::Args::List & args = argParser.args();
		if (!args.size())
		{
			AVM::Engine::printVerbose("Load instructions by stdin");
			AVM::Loader::loadInstructions(vm, std::cin, argv[0]);
		}
		else
		{
			for (PTools::Args::List::iterator it = args.begin() ; it != args.end() ; it++)
			{
				std::string str = "Load instructions by file : ";
				str += it->c_str();
				AVM::Engine::printVerbose(str.c_str());
				AVM::Loader::loadFile(vm, it->c_str());
			}
		}
		AVM::Engine::printVerbose("Check labels");
		vm->checkLabels();
		if (argParser['b'])
		{
			if (AVM::Engine::output == "")
				AVM::Engine::output = "a.out";

			AVM::Engine::printVerbose("Binding...");

			std::string str = "Output file is : ";
			str += AVM::Engine::output;
			AVM::Engine::printVerbose(str.c_str());

			vm->convertLabels();
			AVM::Injector::prepare(argv[0]);
			vm->bind(argv[0]);
		}
		else if (argParser['c'])
		{
			if (AVM::Engine::output == "")
				AVM::Engine::output = "avm.bytecode";

			AVM::Engine::printVerbose("Bytecode Compilation");

			std::string str = "Output file is : ";
			str += AVM::Engine::output;
			AVM::Engine::printVerbose(str.c_str());

			vm->convertLabels();
			vm->compile();
		}
		else
		{
			AVM::Engine::printVerbose("Execute");
			vm->execute();
		}
	}
	catch (AVM::Exception const & e)
	{
		std::cerr << COLOR_RED << "ERROR : " << COLOR_RESET << e.what() << std::endl;
		return AVM::exitFailure(vm);
	}

	return AVM::exitSuccess(vm);
}
