#include "Engine.hpp"
#include "TComparisons.hpp"
#include "Injector.hpp"

namespace AVM
{
	bool			Engine::verbose = false;
	std::string		Engine::output = "";
	long			Engine::wMagicBytecode = 0x0a4d56416d726167;
	long			Engine::rMagicBytecode = 0x004d56416d726167;

/******************************************************************************/
// Coplien
/******************************************************************************/
	Engine::Engine() : _carry(CF_NONE), _readBuffer("")	{ }

	Engine::~Engine()
	{
		while (this->_stack.size() > 0)
		{
			delete this->_stack.back();
			this->_stack.pop_back();
		}

		while (this->_instructions.size() > 0)
		{
			delete this->_instructions.front();
			this->_instructions.pop_front();
		}
	}

	Engine::Engine(Engine const & ref)
	{
		*this = ref;
	}

	Engine &	Engine::operator=(Engine const & rhs)
	{
		this->_stack = rhs.getStack();
		this->_instructions = rhs.getInstructions();
		this->_cursor = rhs.getCursor();
		this->_labels = rhs.getLabels();
		this->_carry = rhs.getCarry();
		this->_readBuffer = rhs.getReadBuffer();
		this->_callStack = rhs.getCallStack();
		return (*this);
	}

/******************************************************************************/
// Main public functions
/******************************************************************************/
	void							Engine::printVerbose(char const * msg)
	{
		if (Engine::verbose)
			std::cerr << COLOR_YELLOW << "--- " << msg << " ---" << COLOR_RESET << std::endl;
	}

	void							Engine::addInstruction(std::string const & fileName, long numLine, Instruction const * i)
	{
		try
		{
			this->_instructions.push_back(i);
			const_cast<Instruction *>(i)->setFileName(fileName);
			if (i->getType() == Instruction::Label)
			{
				std::string str = i->getOperandValue();
				if (this->_labels.find(str) != this->_labels.end())
					throw Exception(Exception::DuplicateLabel);
				this->_labels[str] = --this->_instructions.end();
			}
		}
		catch (Exception & e)
		{
			e.setFileName(fileName);
			e.setLine(numLine);
			throw e;
		}
	}

	void							Engine::addInstruction(std::string const & fileName, long numLine, std::string & line)
	{
		try
		{
			Instruction const * i = Parser::createInstruction(numLine, line);
			this->addInstruction(fileName, numLine, i);
		}
		catch (Exception & e)
		{
			e.setFileName(fileName);
			e.setLine(numLine);
			throw e;
		}
	}

	void							Engine::checkLabels(void) const
	{
		InstructionQueue::const_iterator it;
		for (it = this->_instructions.begin() ; it != this->_instructions.end() ; it++)
		{
			if ((*it)->getType() >= Instruction::Jmp)
			{
				std::string const & label = (*it)->getOperandValue();
				if (this->_labels.find(label) == this->_labels.end())
				{
					Exception e(Exception::UnknowLabel, (*it)->getLine());
					e.setFileName((*it)->getFileName());
					throw e;
				}
			}
		}
	}

	void							Engine::convertLabels(void)
	{
		LabelMap::iterator			label_it;
		InstructionQueue::iterator	inst_it;
		unsigned int 				i = 0;

		for (label_it = this->_labels.begin() ; label_it != this->_labels.end() ; label_it++)
		{
			for (inst_it = this->_instructions.begin() ; inst_it != this->_instructions.end() ; inst_it++)
			{
				if ((*inst_it)->getType() >= Instruction::Label && (*inst_it)->getOperandValue() == label_it->first)
					const_cast<Instruction *>(*inst_it)->setOperandValue(i);
			}
			i++;
		}
	}

	void							Engine::compile(void) const
	{
		std::ofstream						outputFile(Engine::output.c_str());
		InstructionQueue::const_iterator 	it;

		if (!outputFile)
			throw (Exception(Exception::FileWriteError, 0, Engine::output.c_str()));
		
		outputFile.write(reinterpret_cast<const char *>(&Engine::wMagicBytecode), sizeof(long));
		for (it = this->_instructions.begin() ; it != this->_instructions.end() ; it++)
			**it >> outputFile;

		outputFile.close();
	}

	void							Engine::bind(const char *progName) const
	{
		std::ifstream						ifs(progName, std::ios::binary);
		std::ofstream						ofs(Engine::output.c_str(), std::ios::binary);
		std::stringstream					ss;
		InstructionQueue::const_iterator	it;

		if (!ifs)
			throw Exception(Exception::FileError);
		if (!ofs)
			throw Exception(Exception::FileWriteError);

		ofs << ifs.rdbuf();

		if (chmod(Engine::output.c_str(), S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) != 0)
			throw Exception(Exception::FatalError, 0, "chmod error");

		ss.write(reinterpret_cast<const char *>(&Engine::wMagicBytecode), sizeof(long));
		for (it = this->_instructions.begin() ; it != this->_instructions.end() ; it++)
			**it >> ss;

		unsigned int size = static_cast<unsigned int>(ss.tellp());

		if (size >= INJECTION_SIZE)
			throw Exception(Exception::FatalError, 0, "unable to bind : too many bytecodes");

		ofs.seekp(Injector::offset);
		ofs.write(reinterpret_cast<const char *>(&size), sizeof(unsigned int));
		ofs << ss.rdbuf();

		ofs.close();
		ifs.close();
	}

	void							Engine::execute(void)
	{
		typedef void				(Engine::*execFunc)(void);

		bool						exit = false;
		Instruction::type			instructionType;
		execFunc					exec[Instruction::nbType];

		exec[Instruction::Push] = &Engine::push;
		exec[Instruction::Label] = &Engine::checkCallStack;
		exec[Instruction::Pop] = &Engine::pop;
		exec[Instruction::Dump] = &Engine::dump;
		exec[Instruction::Assert] = &Engine::assert;
		exec[Instruction::Add] = &Engine::add;
		exec[Instruction::Sub] = &Engine::sub;
		exec[Instruction::Mul] = &Engine::mul;
		exec[Instruction::Div] = &Engine::div;
		exec[Instruction::Mod] = &Engine::mod;
		exec[Instruction::Print] = &Engine::print;
		exec[Instruction::Exit] = &Engine::exit;

		exec[Instruction::Putn] = &Engine::putn;
		exec[Instruction::Put] = &Engine::put;
		exec[Instruction::Swap] = &Engine::swap;
		exec[Instruction::Dup] = &Engine::dup;
		exec[Instruction::Read] = &Engine::read;

		exec[Instruction::Cmp] = &Engine::cmp;
		exec[Instruction::Jmp] = &Engine::jmp;
		exec[Instruction::Call] = &Engine::call;

		exec[Instruction::Je] = &Engine::je;
		exec[Instruction::Jne] = &Engine::jne;
		exec[Instruction::Jlt] = &Engine::jlt;
		exec[Instruction::Jgt] = &Engine::jgt;
		exec[Instruction::Jle] = &Engine::jle;
		exec[Instruction::Jge] = &Engine::jge;

		exec[Instruction::Jc] = &Engine::jc;
		exec[Instruction::Jnc] = &Engine::jnc;
		exec[Instruction::Jse] = &Engine::jse;
		exec[Instruction::Jso] = &Engine::jso;
		exec[Instruction::Js] = &Engine::js;
		exec[Instruction::Jeof] = &Engine::jeof;

		this->_cursor = this->_instructions.begin();

		while (!exit && this->_cursor != this->_instructions.end())
		{
			instructionType = (*this->_cursor)->getType();
			if (instructionType == Instruction::Exit)
				exit = true;

			try
			{
				(this->*exec[instructionType])();
			}
			catch (Exception & e)
			{
				e.setLine((*this->_cursor)->getLine());
				e.setFileName((*this->_cursor)->getFileName());
				throw e;
			}

			this->_cursor++;
			if (this->_cursor == this->_instructions.end() && this->_callStack.size() > 0)
			{
				this->checkCallStack();
				this->_cursor++;
			}
		}
		if (!exit)
			throw (RunTimeException(RunTimeException::NoExit));
	}

/******************************************************************************/
// Getters
/******************************************************************************/
	Engine::OperandStack				Engine::getStack(void) const { return this->_stack; }
	Engine::InstructionQueue			Engine::getInstructions(void) const { return this->_instructions; }
	Engine::InstructionQueue::iterator	Engine::getCursor(void) const { return this->_cursor; }
	Engine::LabelMap					Engine::getLabels(void) const { return this->_labels; }
	Engine::CarryFlag					Engine::getCarry(void) const { return this->_carry; }
	std::string const &					Engine::getReadBuffer(void) const { return this->_readBuffer; }
	Engine::CallStack const &			Engine::getCallStack(void) const { return this->_callStack; }

/******************************************************************************/
// Private functions
/******************************************************************************/
	void							Engine::checkCallStack(void)
	{
		if (this->_callStack.size() > 0)
		{
			this->_cursor = this->_callStack.back();
			this->_callStack.pop_back();
		}
	}

	void							Engine::push(void)
	{
		Instruction const * i = *this->_cursor;
		IOperand const * o = this->_factory.createOperand(i->getOperandType(), i->getOperandValue());
		this->_stack.push_back(o);
	}

	void							Engine::pop(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		delete this->_stack.back();
		this->_stack.pop_back();
	}

	void							Engine::dump(void)
	{
		OperandStack::reverse_iterator it;
		for (it = this->_stack.rbegin() ; it != this->_stack.rend() ; it++ )
			std::cout << (*it)->toString() << std::endl;
	}
	
	void							Engine::assert(void)
	{
		std::stringstream 		ss;
		Instruction const *		i = *this->_cursor;
		IOperand const * 		assertOperand = this->_factory.createOperand(i->getOperandType(), i->getOperandValue());
		IOperand const * 		topOperand = this->_stack.back();

		std::string				types[nbOperandType];
		types[Int8] = "int8(";
		types[Int16] = "int16(";
		types[Int32] = "int32(";
		types[Float] = "float(";
		types[Double] = "double(";

		if (assertOperand->getType() != topOperand->getType() || assertOperand->toString() != topOperand->toString())
		{
			ss << types[assertOperand->getType()] << assertOperand->toString() << ")";
			ss << " != " << types[topOperand->getType()] << topOperand->toString() << ")";
			delete assertOperand;
			throw (RunTimeException(RunTimeException::Assert, 0, ss.str()));
		}
		delete assertOperand;
	}

	void							Engine::add(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if (this->_stack.size() == 1)
			throw (RunTimeException(RunTimeException::InsufficientOperands));

		OperandStack::reverse_iterator v1 = this->_stack.rbegin();
		OperandStack::reverse_iterator v2 = this->_stack.rbegin();
		v1++;

		IOperand const * result = **v1 + **v2;

		this->pop();
		this->pop();
		this->_stack.push_back(result);
	}

	void							Engine::sub(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if (this->_stack.size() == 1)
			throw (RunTimeException(RunTimeException::InsufficientOperands));

		OperandStack::reverse_iterator v1 = this->_stack.rbegin();
		OperandStack::reverse_iterator v2 = this->_stack.rbegin();
		v1++;

		IOperand const * result = **v1 - **v2;

		this->pop();
		this->pop();
		this->_stack.push_back(result);
	}

	void							Engine::mul(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if (this->_stack.size() == 1)
			throw (RunTimeException(RunTimeException::InsufficientOperands));

		OperandStack::reverse_iterator v1 = this->_stack.rbegin();
		OperandStack::reverse_iterator v2 = this->_stack.rbegin();
		v1++;

		IOperand const * result = **v1 * **v2;

		this->pop();
		this->pop();
		this->_stack.push_back(result);
	}

	void							Engine::div(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if (this->_stack.size() == 1)
			throw (RunTimeException(RunTimeException::InsufficientOperands));

		OperandStack::reverse_iterator v1 = this->_stack.rbegin();
		OperandStack::reverse_iterator v2 = this->_stack.rbegin();
		v1++;

		IOperand const * result = **v1 / **v2;

		this->pop();
		this->pop();
		this->_stack.push_back(result);
	}

	void							Engine::mod(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if (this->_stack.size() == 1)
			throw (RunTimeException(RunTimeException::InsufficientOperands));

		OperandStack::reverse_iterator v1 = this->_stack.rbegin();
		OperandStack::reverse_iterator v2 = this->_stack.rbegin();
		v1++;

		IOperand const * result = **v1 % **v2;

		this->pop();
		this->pop();
		this->_stack.push_back(result);
	}

	void							Engine::print(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));

		IOperand const * i = this->_stack.back();

		if (i->getType() != Int8)
			throw (RunTimeException(RunTimeException::Assert, 0, "print error, only int8"));

		std::cout << static_cast<int8_t>(atoi(i->toString().c_str()));
	}

	void							Engine::exit(void)
	{
		Engine::printVerbose("End of execution");
	}

/******************************************************************************/
// Additional private functions (bonus)
/******************************************************************************/
	void							Engine::putn(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		std::cout << (*this->_stack.rbegin())->toString();
	}

	void							Engine::put(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if ((*this->_stack.rbegin())->getType() == Int8)
			this->print();
		else
			this->putn();
	}

	void							Engine::swap(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if (this->_stack.size() == 1)
			throw (RunTimeException(RunTimeException::InsufficientOperands));

		OperandStack::reverse_iterator v1 = this->_stack.rbegin();
		OperandStack::reverse_iterator v2 = this->_stack.rbegin();
		v1++;
		IOperand const * tmp = *v1;
		*v1 = *v2;
		*v2 = tmp;
	}

	void							Engine::dup(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		IOperand const * o = this->_stack.back();
		this->_stack.push_back(this->_factory.createOperand(o->getType(), o->toString()));
	}

	void							Engine::read(void)
	{
		std::string & buf = this->_readBuffer;

		if (buf == "")
		{
			if (!std::getline(std::cin, buf))
			{
				this->_stack.push_back(this->_factory.createOperand(Int8, "-1"));
				return ;
			}
			buf += "\n";
		}

		if ((buf[0] == '-' && !isdigit(buf[1])) || (!isdigit(buf[0]) && buf[0] != '-'))
		{
			int c = static_cast<int>(buf[0]);
			std::stringstream ss;
			ss << c;


			this->_stack.push_back(this->_factory.createOperand(Int8, ss.str()));
			buf.erase(0, 1);
		}
		else
		{
			bool left = false;
			bool point = false;
			bool right = false;
			size_t len;
			for (len = 0 ; len < buf.size(); len++)
			{
				if (len == 0 && buf[len] == '-')
					continue ;
				else if (isdigit(buf[len]) && !point)
					left = true;
				else if (isdigit(buf[len]) && point)
					right = true;
				else if (buf[len] == '.' && !point && left)
					point = true;
				else
					break ;
			}

			if (!len)
				throw (Exception(Exception::FatalError, 0, "read instruction error"));

			if (left && point && right)
			{
				this->_stack.push_back(this->_factory.createOperand(Double, buf.substr(0, len)));
				buf.erase(0, len);
			}
			else if (left)
			{
				this->_stack.push_back(this->_factory.createOperand(Int32, buf.substr(0, len)));
				buf.erase(0, len);
			}
			else
				throw (Exception(Exception::FatalError, 0, "read instruction error"));
		}
	}

	void							Engine::cmp(void)
	{
		typedef bool (*ltFunc)(std::string const &, std::string const &);
		typedef bool (*gtFunc)(std::string const &, std::string const &);

		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if (this->_stack.size() == 1)
			throw (RunTimeException(RunTimeException::InsufficientOperands));

		static ltFunc		ifLower[nbOperandType];
		static gtFunc		ifGreater[nbOperandType];

		if (ifLower[Int8] == 0)
		{
			ifLower[Int8] 		= &Comparisons::lt<int8_t>;
			ifLower[Int16] 		= &Comparisons::lt<int16_t>;
			ifLower[Int32] 		= &Comparisons::lt<int32_t>;
			ifLower[Float] 		= &Comparisons::lt<float>;
			ifLower[Double] 	= &Comparisons::lt<double>;
		}
		if (ifGreater[Int8] == 0)
		{
			ifGreater[Int8] 	= &Comparisons::gt<int8_t>;
			ifGreater[Int16] 	= &Comparisons::gt<int16_t>;
			ifGreater[Int32] 	= &Comparisons::gt<int32_t>;
			ifGreater[Float] 	= &Comparisons::gt<float>;
			ifGreater[Double] 	= &Comparisons::gt<double>;
		}

		OperandStack::reverse_iterator it_v1 = this->_stack.rbegin();
		OperandStack::reverse_iterator it_v2 = this->_stack.rbegin();
		it_v1++;

		IOperand const * v1 = *it_v1;
		IOperand const * v2 = *it_v2;
		eOperandType type = (v1->getPrecision() >= v2->getPrecision()) ? (v1->getType()) : (v2->getType());

		bool lt = ifLower[type](v1->toString(), v2->toString());
		bool gt = ifGreater[type](v1->toString(), v2->toString());
		if (!lt && !gt)
			this->_carry = CF_EQ;
		else if (lt)
			this->_carry = CF_LT;
		else if (gt)
			this->_carry = CF_GT;
		else
			throw (Exception(Exception::FatalError, 0, "cmp error"));
		if (v1->getPrecision() != v2->getPrecision())
			this->_carry = CF_NONE;
	}

	void							Engine::jmp(void)
	{
		this->_cursor = this->_labels[(*this->_cursor)->getOperandValue()];
	}

	void							Engine::call(void)
	{
		this->_callStack.push_back(this->_cursor);
		this->jmp();
	}

	void							Engine::je(void)
	{
		if (this->_carry == CF_EQ)
			this->jmp();
	}

	void							Engine::jne(void)
	{
		if (this->_carry == CF_LT || this->_carry == CF_GT || this->_carry == CF_NONE)
			this->jmp();
	}

	void							Engine::jlt(void)
	{
		if (this->_carry == CF_LT)
			this->jmp();
	}

	void							Engine::jgt(void)
	{
		if (this->_carry == CF_GT)
			this->jmp();
	}

	void							Engine::jle(void)
	{
		if (this->_carry == CF_LT || this->_carry == CF_EQ)
			this->jmp();
	}

	void							Engine::jge(void)
	{
		if (this->_carry == CF_GT || this->_carry == CF_EQ)
			this->jmp();
	}

	void							Engine::jc(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if (this->_stack.back()->getType() == Int8)
			this->jmp();
	}

	void							Engine::jnc(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		if (this->_stack.back()->getType() != Int8)
			this->jmp();
	}

	void							Engine::jse(void)
	{
		if (this->_stack.size() == 0)
			this->jmp();
	}

	void							Engine::jso(void)
	{
		if (this->_stack.size() == 1)
			this->jmp();
	}

	void							Engine::js(void)
	{
		if (this->_stack.size() >= 2)
			this->jmp();
	}

	void							Engine::jeof(void)
	{
		if (this->_stack.size() == 0)
			throw (RunTimeException(RunTimeException::EmptyStack));
		IOperand const * o = this->_stack.back();
		if (o->getType() == Int8 && o->toString() == "-1")
			this->jmp();
	}
}
