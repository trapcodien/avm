#usage: ./avm [-vcbh] [-o outfile] [file1 ...]

###Abstract VM help :###

	-v / --verbose			Verbose Mode.

	-c / --compile			Compile AVM scripts in bytecodes (default output: avm.bytecode).

	-b / --bind				Use AVM binary as a stub for make a fully working executable (default output: a.out).

	-h / --help				Display this help.

	-o / --outfile			Change the default output file.

